import java.util.Scanner;
public class D4_56604_Rajat {
    public static void main(String [] args)
    {
        Scanner sc = new Scanner(System.in);
        Stack stack = new Stack();
        boolean status = true;
        while(status)
        {
            System.out.println("Enter option for operations on book\n 0.exit\n 1.push\n 2.pop\n 3.top\n");
            int choice = sc.nextInt();
            switch(choice)
            {
                case 0:
                status = false;
                case 1:
                System.out.println("Enter book details");
                System.out.println("Enter name");
                String name = sc.next();
                System.out.println("Enter author name");
                String author = sc.next();
                System.out.println("Enter no. of pages");
                int pages = sc.nextInt();
                System.out.println("Enter price");
                double price = sc.nextDouble();
                stack.push(new Book(name, author, pages, price));
                System.out.println("Added successfully");
                break;
                case 2:
                stack.pop();
                System.out.println("Deleted successfully");
                break;
                case 3:
                stack.top();
                break;
                default:
                System.out.println("Enter valid option");
                break;
            }
        }
        sc.close();
    }
}

class DoublyLinearLL
{
    static class Node{
        private Node prev;
        private Book book; 
        private Node next;

        public Node(Book book)
        {
            this.book = book;
            this.prev = null;
            this.next = null;
        }
    }

    private Node head;
    private int node_cnt;

    public DoublyLinearLL()
    {
        this.head = null;
        this.node_cnt = 0;
    }

    public boolean is_Empty()
    {
        return(head==null);
    }

    public boolean is_Full()
    {
        return(node_cnt==5);
    }

    public void add_First(Book book)
    {
        Node newNode = new Node(book);
        if(head==null)
        {
            newNode.prev = null;
            newNode.next = null;
            head = newNode;
            node_cnt++;
        }
        else
        {
            newNode.prev = null;
            newNode.next = head;
            head.prev = newNode;
            head = newNode;
            node_cnt++; 
        }
    }

    public void delete_First()
    {
        if(head.next==null)
        {
            head = null;
            node_cnt--;
        }
        else{
            head.next.prev = null;
            head = head.next;
            node_cnt--;
        }
    }

    public void diplay_First()
    {
        head.book.displayInfo();
    }
}

class Stack
{
    private DoublyLinearLL dl;

    public Stack()
    {
        dl = new DoublyLinearLL();
    }

    public void push(Book book)
    {
        if(!dl.is_Full())
        {
            dl.add_First(book);
        }
        else{
            System.out.println("Stack is full");
        }
    }

    public void pop()
    {
        if(!dl.is_Empty())
        {
            dl.delete_First();
        }
        else
        {
            System.out.println("Stack is empty");
        }
    }

    public void top()
    {
        if(!dl.is_Empty())
        {
            dl.diplay_First();
        }
        else
        {
            System.out.println("Stack is empty");
        }
    }
}

class Book
{
    private String name;
    private String author;
    private int no_of_pages;
    private double price;

    public Book(String name, String author, int no_of_pages, double price)
    {
        this.name = name;
        this.author = author;
        this.no_of_pages = no_of_pages;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getNo_of_pages() {
        return no_of_pages;
    }

    public void setNo_of_pages(int no_of_pages) {
        this.no_of_pages = no_of_pages;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void displayInfo()
    {
        System.out.println("book [author=" + author + ", name=" + name + ", no_of_pages=" + no_of_pages + ", price=" + price + "]");
    }

    
}